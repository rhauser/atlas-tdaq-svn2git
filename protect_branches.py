#!/usr/bin/env python

import sys, os, gitlab, json

git   = gitlab.Gitlab("gitlab.cern.ch", token=os.environ['GITTOKEN'])
group = os.environ['GITGROUP']

for line in open('PACKAGES').readlines():
    pkg, rest = line.split()
    prj = [ p['id'] for p in git.searchproject(pkg) if p['path_with_namespace'] == group+'/'+pkg ]
    if prj:
      branches = git.getbranches(prj[0])
      print "Found ",len(branches),"in ",pkg
      for br in branches:
         name = br['name']
         if name != 'master':
            print "Protecting",name
            git.protectrepositorybranch(prj[0],name)
    else:
      print "Package not found:",pkg
       


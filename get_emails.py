#!/usr/bin/env python
#!/usr/bin/env python

import sys, os, gitlab, string

if len(sys.argv) != 2:
    print >>sys.stderr, "Usage: get_emails.py <project_name>"
    sys.exit(1)

git   = gitlab.Gitlab("gitlab.cern.ch", token=os.environ['GITTOKEN'])
group = os.environ['GITGROUP']

try:
    prj_id = [ p['id'] for p in git.searchproject(sys.argv[1]) if p['path_with_namespace'] == group + '/' + sys.argv[1] ][0]
except:
    print >>sys.stderr,"get_mails.py: Can't find project in group %s: %s" % (group,sys.argv[1])
    sys.exit(1)

emails = []

for m in git.getprojectmembers(prj_id):
    try:
        emails.append(os.popen('phonebook --login %s -t email' % m['username']).read().split(';')[0])
    except:
        print >>sys.stderr,"get_emails.py: Can't find user's e-mail address in phone book: %s" % m['username']

print string.join(emails,',')

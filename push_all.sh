#!/bin/bash
while read pkg svnroot
do
    if [ -d ${pkg}/.git ]
    then
        (cd ${pkg}; git remote | grep -q origin)
        if [ $? -ne 0 ]
        then
            (cd ${pkg} && git remote add origin ${GITROOT}/${GITGROUP}/${pkg}.git)        
        fi
        (cd ${pkg} && git push --all -u && git push --tags)
    else
        echo "${pkg} is not a valid git repository, ignoring it"
    fi
done < PACKAGES

#
# Bash setup script for SVN to GIT conversion scripts.
# You should either explicitly enter the GITGROUP and GITTOKEN
# variables here, or set them in your environment by other means.
#
# If GITTOKEN is not set and there is a TOKEN file in the script
# directory, that is read in as value.
#
# Note that GITTOKEN allows access to your gitlab project, so you
# should probably keep this file private.
#
# Default CERN gitlab root
export GITROOT=${GITROOT:=ssh://git@gitlab.cern.ch:7999}

# Should be customized by user and per project conversion
export GITGROUP=${GITGROUP:=atlas-tdaq-software}

# The base of your SVN directory
export SVNROOT=${SVNROOT:=svn+ssh://svn.cern.ch/reps/atlastdaq}

# GITTOKEN=

# Location of our tools
export SVN2GIT_ROOT=$(readlink -f $(dirname $BASH_SOURCE[0]))
export PATH=${SVN2GIT_ROOT}:${PATH}

# Read GITTOKEN from file if not available
if [ -z "${GITTOKEN}" -a -f ${SVN2GIT_ROOT}/TOKEN ]; then
    read -r GITTOKEN < ${SVN2GIT_ROOT}/TOKEN
    export GITTOKEN
fi

if [ -z "${GITTOKEN}" ]; then
    echo "Warning: $GITTOKEN is not defined, some commands may not work, e.g. create_repositories"
fi

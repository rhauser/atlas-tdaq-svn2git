#!/usr/bin/env python
#
# Create the groups in gitlab.cern.ch based on:
# 
# PACKAGES file in current directory, format:
# <package_name> <full_svn_path>
#
# GITGROUP environment variable contains your gitlab group.
# GITTOKEN environment variable contains your gitlab access token.
#
# You must have access to the group. Existing projects are not overridden,
# but ignored, so you can run this script multiple times if you keep
# updating your PACKAGES file.
#
import sys
import os
import gitlab

git   = gitlab.Gitlab("gitlab.cern.ch", token=os.environ['GITTOKEN'])
group = os.environ['GITGROUP']

for g in git.getgroups():
    if group == g['name']:
        group = g
        break
else:
    print "Group not found:",sys.argv[1]
    sys.exit(1)

for line in len(sys.argv) > 1 and sys.argv[1:] or open('PACKAGES').readlines():
    pkg = line.split()[0]
    p = git.createproject(pkg, namespace_id=group['id'], path=pkg)
    if p:
        print pkg, p
    else:
        print pkg, "EXISTS"

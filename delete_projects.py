#!/usr/bin/env python
#
# Delete the projects  in gitlab.cern.ch based on:
# 
# PACKAGES file in current directory, format:
# <package_name> <full_svn_path>
#
# Alternatively a list of project names can be specified
# explicitly on the command line.
#
# delete_projects.py prj1 prj2...
#
# GITGROUP environment variable contains your gitlab group.
# GITTOKEN environment variable contains your gitlab access token.
#
# You must have access to the group. Yes, projects are *really*
# deleted and there is no way to retrieve them, so be careful.
#
# You do have another copy of them, do you ? Do you ?
#
import sys
import os
import gitlab
import json

git   = gitlab.Gitlab("gitlab.cern.ch", token=os.environ['GITTOKEN'])
group = os.environ['GITGROUP']

for g in git.getgroups():
    if group == g['name']:
        group = g
        break
else:
    print "Group not found:",sys.argv[1]
    sys.exit(1)

def delete_project(pkg):
    p = git.getproject(group['name'] + '/' + pkg)
    if p:
        git.deleteproject(p['id'])
    else:
        print pkg, "does NOT exist."

if len(sys.argv) > 1:
    packages = sys.argv[1:]
else:
    packages = [ line.split()[0] for line in open('PACKAGES').readlines() ]

print "About to delete these packages: "
print packages
print 
print "Type Enter to continue or hit Ctrl-C to abort now..."
sys.stdin.readline()
for pkg in packages:
    delete_project(pkg)

#!/bin/bash
#
# list_packages.sh [ release/version1 release/version2... ]
#
# Get a list of all TDAQ project related packages.
#
# This relies on the TDAQ specific tag collector information.
# The output is a file called PACKAGES in the current
# directory with this format:
#
# <package_name> <full_svn_path>
#
# This is used as input for the next step in the conversion.
# You can create the PACKAGE file in any way
# you like or which is convenient for you (including manually).
#
# Note that the output is merged with an existing PACKAGE file.
#
# Customization:
#
# This is the ATLAS TDAQ specific repository root.
SVNROOT=${SVNROOT:=svn+ssh://svn.cern.ch/reps/atlastdaq}

# This is the AFS directory with ATLAS TDAQ specific tags.
TAGCOLLECTOR_ROOT=${TAGCOLLECTOR_ROOT:=/afs/cern.ch/atlas/project/tdaq/cmt/adm/packages/}

if [ $# -gt 0 ]; then
    RELEASES="$*"
else
    
# This is the name of the releases we want to include in our list:
#
#    RELEASES=${RELEASES:="tdaq-common/tdaq-common-01-31-00 tdaq-common/tdaq-common-nightly"}
#    RELEASES=${RELEASES:="dqm-common/dqm-common-00-37-00 dqm-common/dqm-common-nightly"}
    RELEASES=${RELEASES:="tdaq/tdaq-05-05-00 tdaq/nightly"}
fi

# Copy existing file, if it exists.
cp PACKAGES PACKAGES.tmp 2> /dev/null

for release in ${RELEASES}
do
    for file in ${TAGCOLLECTOR_ROOT}/${release}/*
    do
        pkg=$(basename ${file})
        relpath=$(head -1 ${file})
        echo "${pkg} ${SVNROOT}/${relpath}/${pkg}" 
    done 
done > PACKAGES.tmp

sort -u PACKAGES.tmp > PACKAGES
rm -f PACKAGES.tmp
